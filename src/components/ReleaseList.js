import React from 'react'
import '../App.css'

const ReleaseList = ({count, releases, includePrerelease}) => {
  if (releases.length === 0) {
    return null
  }
  if (!includePrerelease) {
    releases = releases.filter(r => !r.isPrerelease)
  }

  let rows = releases.map(r => <tr key={r.name}>
                                  <td>{r.name}</td>
                                  <td>{(new Date(r.publishedAt)).toDateString()}</td>
                                </tr>)
  return  <table>
            <thead>
              <tr>
                <td>Name</td>
                <td>Published On</td>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
}

export default ReleaseList