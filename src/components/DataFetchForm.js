import React, {Component} from 'react'


class DataFetchForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      releaseCount: props.releaseCount,
      onFetch: props.onFetch,
      token: '',
    }
  }

  onChange = event => this.setState({...this.state, token: event.target.value})

  onSubmit = event => {
    this.state.onFetch(this.state.token)
    event.preventDefault()
  }

  render = () => {
    return (
        <form onSubmit={this.onSubmit}>
        <label>
          Auth token:
          <input type="password" value={this.state.token} onChange={this.onChange}/>
        </label>
        <input type="submit" value="Fetch" disabled={this.state.token === ''}/>
      </form>
    )
  }
}

export default DataFetchForm