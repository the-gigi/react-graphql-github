import React, {Component} from 'react'
import './App.css'
import ReleaseList from "./components/ReleaseList"
import DataFetchForm from "./components/DataFetchForm"
import axios from 'axios'

const githubApiUrl = 'https://api.github.com/graphql'
const releaseCount = 10

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      releases: []
    }
  }

  onFetch = (token) => {
    const query = `
      query { 
        repository(name: kubernetes, owner: kubernetes) {
          releases(first: ${releaseCount}, orderBy: { field: CREATED_AT, direction: DESC}) {
            totalCount
            nodes {
              name
              publishedAt
              isPrerelease
            }
          }
        }
      }
    `

    axios.post(githubApiUrl, JSON.stringify({query}), {headers: {Authorization: `bearer ${token}`}})
      .then(r => {
        this.setState({...this.state, releases: r.data.data.repository.releases.nodes})
      })
      .catch(e => console.log(e))
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">React + GraphQL + Github API Demo App</h1>
        </header>
        <h2 className="App-intro">
          Kubernetes Releases
        </h2>
        <DataFetchForm onFetch={this.onFetch}/>
        <ReleaseList count={releaseCount} releases={this.state.releases} includePrerelease={false}/>
      </div>
    )
  }
}

export default App
